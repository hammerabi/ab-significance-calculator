# Project Requirements

## Create a Django application that calculates the statistical significance between two A/B testing sets of data

### Steps:

- Create Django application with support for Heroku
- Project must have Django 1.11, Python 3.5, and Postgres 9.4+ support
- Responsive landing page with a form that takes 4 user inputs, and returns a P-Value + boolean whether the result is statistically significant
- Well designed.

### Josh Butcher - 10-10-2017


TODO-JB: Update readme with build steps and add a deploy script for Heroku
