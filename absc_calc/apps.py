from django.apps import AppConfig


class AbscCalcConfig(AppConfig):
    name = 'absc_calc'
