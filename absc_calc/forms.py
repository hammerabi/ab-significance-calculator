from django import forms

class CalculatorForm(forms.Form):
    control_visitors = forms.CharField(label="Control Visitors", widget=forms.TextInput(attrs={'class': 'form-control'}))
    control_conversions = forms.CharField(label="Control Conversions", widget=forms.TextInput(attrs={'class': 'form-control'}))
    variation_visitors = forms.CharField(label="Variation Visitors", widget=forms.TextInput(attrs={'class': 'form-control'}))
    variation_conversions = forms.CharField(label="Variation Conversions", widget=forms.TextInput(attrs={'class': 'form-control'}))
