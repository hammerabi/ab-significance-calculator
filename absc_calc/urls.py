from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt, csrf_protect

from . import views

urlpatterns = [
    url(r'^$', views.calculate_statistical_significance, name='calculate'),
]
