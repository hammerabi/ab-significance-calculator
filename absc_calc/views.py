from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_protect
from .forms import CalculatorForm
import datetime

# [GET] Landing page that renders form with input fields
def landing_view(request):
    if request.method == 'POST':
        form = CalculatorForm(request.POST)
        if form.is_valid():
            return JsonResponse({'hey': 'there'})
    else:
        form = CalculatorForm()
    return render(request, "home.html", {'form': form})
