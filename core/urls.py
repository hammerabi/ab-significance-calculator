from django.conf.urls import include, url
from django.contrib import admin
from absc_calc import views as v

urlpatterns = [
    #url(r'^calculate/', include('absc_calc.urls')),
    url(r'^$', v.landing_view, name='home'),
    url(r'^admin/', admin.site.urls),
]
